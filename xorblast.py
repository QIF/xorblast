#!/usr/bin/python3
from functools import reduce
import argparse
import sys
from io import StringIO
import numpy as np

__author__ = "Alexander Weigl <weigl@kit.edu>"
__date__ = "2015-10-23"
__version__ = "0.8.2"


def build_command_parser():
    """
    """
    ap = argparse.ArgumentParser(
        description="This tool find CBMC information about variables in DIMACS files and provide a way to project on specific variables.",
        epilog="""
Usage example:
  python3 xorblasting.py test.cnf

You can specify @name as a variable name to get project on all time.
  """)

    ap.add_argument("--no-gauss", action="store_true", default=False, help="")
    ap.add_argument("--smt-output", type=str, default=None,
                    help="if given, a proof obligation for the xor constraints is written out.")
    ap.add_argument("-v", "--verbose", action="store_true", default=False)
    ap.add_argument("-o", "--output", metavar="FILE", action="store",
                    help="write a new cnf file to the given FILE")
    ap.add_argument("file", metavar="DICOM",
                    help="input cnf file, with CBMC comments")

    return ap


def write_smt(xorclauses_old, xorclauses_new, filename):
    declared = set()

    def tosexpr(literal):
        if literal > 0:
            return "A%d" % literal
        else:
            return "(not A%d)" % (-1 * literal)

    with  open(filename, 'w') as fh:
        fh.write("(set-option :produce-models true)\n")
        for xorclauses in (xorclauses_old, xorclauses_new):
            for clause in xorclauses:
                for literal in clause:
                    if abs(literal) not in declared:
                        fh.write("(declare-const A%d Bool)\n" % abs(literal))
                        declared.add(abs(literal))

        fh.write("(assert (not (= \n")

        for xorclauses in (xorclauses_old, xorclauses_new):
            fh.write("\t(and \n")
            for clause in xorclauses:
                if len(clause) == 0:
                    continue
                elif len(clause) == 1:
                    fh.write("\t\t%s\n" % tosexpr(clause[0]))
                else:
                    fh.write("\t\t(xor %s)\n" % ' '.join(map(tosexpr, clause)))
            fh.write("\t)")

        fh.write(")))  \n(check-sat)\n(get-model)")


def xorblast(xor_clauses, max_var, clauses, out):
    """
    :param literals: list of integers (literal)
    :param max_var:  current maximal variable
    :param clauses:  current count of clauses
    :param out:
    :return:
    """
    var = max_var

    def tseitin(a, b):
        """ a + b + c ....
           z <-> a + b
        => z <-> (a \/ b) /\ (a \/ b)
        =>
           z ->  (a \/ b) /\ (-a \/ -b)
           => -z \/ (a \/ b) /\ (-a \/ -b)
           => (-z \/ a \/ b) /\ (-z \/ -a \/ -b)
        => (a \/ b) /\ (-a \/ -b) -> z
           => (a \/ b) /\ (-a \/ -b) \/  z
           => (a \/ b \/ z ) /\ (-a \/ -b \/  z)
        """
        nonlocal var, clauses
        clauses += 4
        z = var + 1
        var = z

        for clause in [(-z, a, b), (-z, -a, -b), (z, -a, b), (z, a, -b)]:
            out.write(" ".join(map(str, clause)))
            out.write(" 0 \n")

        return z

    for literals in xor_clauses:
        if len(literals) == 0:
            continue
        elif len(literals) == 1:
            out.write(str(literals[0]) + " 0\n")
        else:
            out.write("c ---- " + str(literals) + "\n")
            var = reduce(tseitin, literals)
            out.write("%d 0\n" % var)

    return var, clauses


def create_mapping(xor_clauses):
    map = set()

    for c in xor_clauses:
        for l in c:
            map.add(abs(l))

    c2v = list(map)
    v2c = {l: i for i, l in enumerate(c2v)}

    return c2v, v2c


def gjel(A):
    """Gauss-Jordan elimination."""
    A = np.copy(A)  # explictly make a copy of A

    def swap_rows(frm, to):
        A[[frm, to], :] = A[[to, frm], :]

    m, n = (len(A), len(A[0]))

    for i in range(m):
        # region Find row with pivot element and swap
        pivot = A[i:, i].argmax() + i
        if pivot != i:
            # print(A)
            swap_rows(i, pivot)
            # print(A)
        # endregion pivot swap end

        if A[i, i] == 0:
            break  # raise ArithmeticError("pivot element is zero")

        # normalize row
        row = A[i]  # / A[i, i] # this is an idempoten operator 1 / 1 = 1, 0 / 1 = 0

        # propagate to bottom
        for j in range(i + 1, len(A)):
            # [ 1 1 0 0] = a
            # [ 0 1 1 0] = b
            # A[j] -
            A[j] = (A[j] - row * A[j, i]) % 2

    # back propagate

    # [2 6 3]
    # [0 3 2]
    # A[j] / (6 * 3) -

    for i in range(m - 1, -1, -1):
        for j in range(i - 1, -1, -1):
            if A[j, i] == 1:
                A[j] = (A[j] - A[i]) % 2

    return A


def check_xor_for_unsat(A):
    """

    :param A:
    :return:
    """
    for row in A:
        all_zeros = all(map(lambda x: x == 0, row[:-1]))
        last_zeros = row[-1] == 0

        if all_zeros and not last_zeros:
            raise ArithmeticError("Matrix is not solvable")


def xor_gauss_elim(xor_clauses,  smt_output = None):
    # Create a equation system

    # we need to map between columns and variables
    c2v, v2c = create_mapping(xor_clauses)

    m = len(xor_clauses)
    n = len(c2v)

    # +1 for the result vector
    # A x = b ==> [A b]
    A = np.zeros((m, n + 1))

    # decode xor sets into matrix form
    # if variable occur A[row, var] = 1
    for row, clause in enumerate(xor_clauses):
        for literal in clause:
            col = v2c[abs(literal)]
            A[row, col] = 1

    # If xor clause is positive ( a + b = 1 ) then last column has a one
    # Else leave unchanged (0), Hint: (a+b=0) iff (a == b)
    for row, clause in enumerate(xor_clauses):
        if clause[0] > 0:
            A[row, -1] = 1

    # print("A=matrix(GF(2), %r)" % A, file=sys.stderr) # debug

    # solve the system, try the best
    B = gjel(A)

    # checks for a matrix row like [0 ... 0 1] which is unsat.
    check_xor_for_unsat(B)  # throws exception if unsat

    # print(B, file=sys.stderr) # debug

    # Gauß Elimination not successfull,
    # Imagine case where a + b = 0 /\ a + b = 1
    # I believe that, this is the right flag
    # if nulldim != 0:
    #    print("XOR clauses unsatisfiable found with Gauß Elimination", file=sys.stderr)
    #    sys.exit(1)

    # Tranform matrix back to set of clauses.
    new_xor_clauses = []
    for row in B:
        clause = []
        for column, value in enumerate(row[:-1]):  # ignore last column
            if value == 1:  # variable survived
                clause.append(c2v[column])

        # if last column is zero,
        # then the row express ( a + ... + z = 0)
        #  then first entry must be false
        if row[-1] == 0 and clause:
            clause[0] *= -1

        new_xor_clauses.append(clause)

    if smt_output:
        write_smt(xor_clauses, new_xor_clauses, smt_output)

    return new_xor_clauses

    # for row in B:
    #     for col, val in enumerate(row[:-1]):
    #         l = (1 if val else -1) * c2v[col]
    #         output_handle.write(str(l))
    #         output_handle.write(" ")
    #     output_handle.write("\n")


def analyze_file(fp, output_file=sys.stdout, gauss=True, smt_output = None):
    """Extract the variable information from the given

    :param filename:
    :return:
    """

    clauses = 0
    variables = 0

    output = StringIO()

    xor_clauses = []

    for line in fp.readlines():
        if line[0] == 'p':
            def toint(x):
                try:
                    return int(x)
                except:
                    return None

            _, _, variables, clauses = map(toint, line.split(' '))
            # variables += 1
            continue

        if line[0] == 'x':  # found xor constraint
            output.write("c found:" + line)
            xor_clauses.append(line[1:])
            # variables, clauses = xorblast(pclause(line[1:]),
            #                     variables,
            #                     clauses,
            #                     output)

            # output.write(str(variables) + " 0\n")
            # clauses +=1

        else:
            output.write(line)

    if xor_clauses:  # if xor clauses found
        # transform string literals to integers
        xor_clauses = [pclause(clause) for clause in xor_clauses]

        # remove trailing 0 literals
        for xorc in xor_clauses:
            if xorc[-1] == 0:
                del xorc[-1]

        # if requested make gauss elimination
        if gauss:
            xor_clauses = xor_gauss_elim(xor_clauses, smt_output=smt_output)

        # Do the Tseitin
        variables, clauses = xorblast(xor_clauses, variables, clauses, output)

    output_file.write("p cnf %d %d\n" % (variables, clauses))
    output_file.write(output.getvalue())


def pclause(string):
    return list(map(int, filter(lambda x: "" != x.strip(" \t\n"), string.split(' '))))


def main():
    ap = build_command_parser()
    ns = ap.parse_args()

    with open_(ns.file, 'r') as fi:
        with open_(ns.output, 'w') as fo:
            analyze_file(fi, output_file=fo,
                         gauss=not ns.no_gauss,
                         smt_output=ns.smt_output)


def open_(filename, mode='r'):
    if filename == "-" or filename is None:
        return sys.stdin if mode == 'r' else sys.stdout
    else:
        return open(filename, mode)


def mainpt():
    if "-h" in sys.argv or "--help" in sys.argv:
        print("""Usage: %s <SATSOLVER COMMAND ... > DIMACSFILE

        xorblast-pt blasts the given DIMACSFILE and pipe the new
        file to the given SATSOLVER with the given options.
        """ % sys.argv[0])

        sys.exit(0)

    if len(sys.argv) < 3:
        print("Insufficient number of args.")
        sys.exit(1)

    import subprocess
    filename = sys.argv[-1]

    proc = subprocess.Popen(args=sys.argv[1:-1], stdin=subprocess.PIPE,
                            stdout=sys.stdout,
                            stderr=sys.stderr)

    with open_(filename) as fi:
        analyze_file(fi, proc.stdin)


if __name__ == "__main__":
    main()
