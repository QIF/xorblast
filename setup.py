from distutils.core import setup
from xorblast import __version__

setup(
    name='xorblast',
    version=__version__,
    packages=[''],
    url='http://gitlab.com/QIF/xorblast',
    license='GPL-3.0',
    author='Alexander Weigl',
    author_email='weigl@kit.edu',
    description='Blasting of XOR clauses in DIMACS files.',
    py_module=["xorblast"],
    classifiers=(
        "Development Status :: 5 - Production/Stable",
        "Programming Language :: Python :: 3 :: Only",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
    ),
    entry_points={
        'console_scripts': [
            'xorblast = xorblast:main',
            'xorblast-pt = xorblast:mainpt'
        ], }
)
