#!/bin/sh

CASES="2 4 4 5 6 7 10 15 20 25 32 128 256 1024 4096"

for N in $CASES
do
	./create_test.sh $N
	o=$(/home/weigl/work/dsharp/dsharp  xor${N}e.cnf | grep 'Models counted')
	
	e=$(python -c "print 2**($N-1)")
	expected="Models counted after projection: $e"
	
	if [ "$expected" = "$o" ]; then 
		echo $N ... ok
	else
		echo "$N ... error : $expected <<>> $o"
	fi 
done
