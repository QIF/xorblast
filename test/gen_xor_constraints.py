#!/usr/bin/python3

from argparse import ArgumentParser
import random

R = random.Random()


def select(seq, survival_prob, parity_prob):
    """

    :param seq:
    :param survival_prob:
    :param parity_prob:
    :return:
    """

    def pred(x):
        return R.random() <= survival_prob

    r = list(filter(pred, seq))

    if R.random() <= parity_prob:
        r[0] *= -1
    return r


def main():
    ap = ArgumentParser()
    ap.add_argument("-m", type=int, help="number of xor contraints", default=8)
    ap.add_argument("-n", type=int, help="number of variables", default=32)
    ap.add_argument("-s", type=float, help="survival factor of variable in xor clause", default=0.5)
    ap.add_argument("-p", type=float, help="parity probability", default=0.5)

    ns = ap.parse_args()

    print("p cnf %d 0" % ns.n)
    variables = range(1, ns.n + 1)
    for i in range(ns.m):
        xorcs = select(variables, ns.s, ns.p)
        print("x %s 0" % ' '.join(map(str,xorcs)))

if __name__ == '__main__':
    main()