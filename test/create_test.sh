#!/bin/bash



N=$1

filename="xor${N}.cnf"

echo "p cnf $N 0" > $filename
echo -n "cr "     >> $filename
seq -s ' ' 1 $N       >> $filename
echo -n "x "      >> $filename
seq -s ' ' 1 $N       >> $filename

../xorblast.py --no-gauss $filename > "xor${N}e.cnf"

